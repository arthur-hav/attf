from fastapi import FastAPI, WebSocket, Depends
from fastapi.responses import HTMLResponse
from fastapi.exceptions import WebSocketException
from fastapi.websockets import WebSocketDisconnect
from pydantic import BaseModel
import redis.asyncio as redis
import asyncio
import json
import websockets.client
import logging

from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def conn_redis():
    return redis.Redis(host="redis")


class TileData(BaseModel):
  x : int
  y : int
  flags: list[str]


class GameHandler():
    def __init__(self, r:redis.Redis=Depends(conn_redis)) -> None:
        self.r = r

    async def get_by_id(self, game_id) :
        current_game = await self.r.get(game_id)
        if current_game:
            return json.loads(current_game)
        return await self.create(game_id)
    
    async def create(self, game_id):
        data = {'grid': {x + 1000000 * y: TileData(x=x, y=y, flags=[]).model_dump() for x in range(17) for y in range(17)}, 'creatures': {}, 'onTrait': 0}
        await self.r.set(game_id, json.dumps(data))
        return data

    async def handle_diff(self, game_id: str, line_diff: list) -> tuple[list[str], list[str]]:
        messages = []
        op = line_diff[0]
        key = line_diff[1]
        game = json.loads(await self.r.get(game_id))

        if op == "set":
            if key :
                cur_game = game
                for k in key[:-1]:
                    cur_game = cur_game[str(k)]
                cur_game[str(key[-1])] = line_diff[2]
            else: 
                game = line_diff[2]
            await self.r.set(game_id, json.dumps(game))
        elif op == "unset":
            if key :
                cur_game = game
                for k in key[:-1]:
                    cur_game = cur_game[str(k)]
                del cur_game[str(key[-1])]
                await self.r.set(game_id, json.dumps(game))
            else: 
                await self.r.delete(game_id)
        return [], messages

@app.websocket("/{game_id}")
async def websocket_endpoint(
    websocket: WebSocket,
    game_id: str,
    handler: GameHandler = Depends(GameHandler),
    r: redis.Redis = Depends(conn_redis)
):
    await websocket.accept()
    game = await handler.get_by_id(game_id)

    messages = [["set", [], game]]
    await websocket.send_json(messages)

    p = r.pubsub()
    await p.subscribe("game-update." + game_id)

    async def ws_loop():
        while True:
            data = await websocket.receive_json()
            messages = []
            priv_responses = []
            for diff in data:
                resp, broadcast = await handler.handle_diff(game_id, diff)
                messages.extend(broadcast)
                priv_responses.extend(resp)
                messages.append(diff)
            await asyncio.gather(
                websocket.send_json(priv_responses), r.publish("game-update." + game_id, json.dumps(messages))
            )

    async def rd_loop():
        while True:
            mess = await p.get_message(timeout=None)
            mess = json.loads(mess["data"].decode("utf-8")) if mess and mess["type"] == "message" else None
            if mess:
                await websocket.send_json(mess)

    try:
        await asyncio.gather(ws_loop(), rd_loop())
    except (WebSocketDisconnect, WebSocketException):
        pass
    finally:
        await p.unsubscribe("game-update." + game_id)


import uvicorn

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000, log_level='debug')