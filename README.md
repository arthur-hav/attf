# attf

## What is this

Attf is a tabletop framework that will set the basic rules, especially combat rules, for a dice-less deeply tactical role playing experience.

Attf is agnostic of scenery (anything from sci fi to heroic fantasy scenery) and will leave also other areas such as gear and player progress down in the hands of the game master.

## Combat rules

The combat is done in turn-based fashion over a hex grid, in the order of initiative. Higher initiative act first. On ties, characters with highest max hp act first; and dm choice on tie for max hp.

Players get to see what's on their field of view, then are proposed to do any of:
- Delaying their turn, skipping it entirely and diminisihng their initiative for the reminder of combat by an amount of their choice (initiative can be negative)
- Apply every on-turn effect such as damage over time, regaining their energy etc.
- Spend movement and action points according to their will.
- End their turn by giving the direction which they are facing.


### The map

Terrain can have various features.

Every tile can contain a single character. It can be:

- High ground or low ground. Characters can't melee fight over a ground difference, and shots fired from lower ground gets a bonus defenese.
- Obstructing wall. Characters can't move or see through an obstructing wall.
- Fog. Players can move through but can't see through fog.
- Difficult terrain. Players need twice more movement to move from it.
- Other conditino, dictated by special effect : trap, daamge over time, healing beacon...

Additionally, separators between tow tiles can also be:

- Cover advantage. THe advantage is yielded by anyone next to a cover delimiter against anyone attempting to shoot through it. 
The cover advantage is not granted if the shooter is next to the cover. Note that if both are standing next to it, none get the cover advantage.
- Difficult terrain. Any movement going through the delimiter asks for an extra movement point.
- Obstructing wall. Characters can't move or see through an obstructing wall.
- Other conditino, dictated by special effect


### Attributes

Every character hold the given characteristics:

- HP (current and max). When they reach 0 you are (immediately) dead.
- Energy points (current, max, per turn). They are used to cast specials.
- Combat skill slots. They are used to learn specials and martial art.
- Movement points. Used to move every turn. 1 point = 1 tile movement
- Action points. Used to do actions. See actions.
- Base attack damage. Used to compute damage from melee or ranged attacks.
- Base range.
- Base special power. Used to compute the magnitude of effect for specials.
- Perception range. How far can someone see.
- Armor. Reduce incoming attack damage.
- Endurance. Reduce incoming special power.
- Initiative. How early a character can act.
- Backstab bonus damage %. How much more a backstab deals

We recommend the following base starting characteristics:

- 25/25 Health 
- 25/25, +2/t Energy
- 1 Movement point
- 3 Action point
- 5 AD for melee, 4 for ranged
- 1 range for melee, 5 for ranged
- 1 Special (prepared slot)
- 5 Special power
- 6 Perception
- 0 Armor
- 0 Endurance
- 5 Initiative
- 50% backstab bonus

These characteristics can be modified based on gear, based on players background (classes). A pure fighter might be granted 35 health. A pure mage might be granted 2 prepared slots. A rogue can have better backstab bonus. A given gear might give a few temporary hit points, 1-2 armor or endurance, 1-2 additional AD, have initiative modifiers.

A ring or amulet might give access to spells, in their own slot or using the character slot.

The recommendation for gears is to keep the number of slots to a reasonably low number. Additionally, players are encouraged to having to choose between melee or distance combat. At game master discretion, players may either be disallowed from changing gear in combat, or doing so for 1 non-armor equipment at the cost of a full turn.

 Here is an example of minimalistic slots for each character:

- Armor set
- Helmet
- Left & right hand for weapon(s) and/or shield
- Ring (x2)
- Amulet

Having a small number of armor slots allow you to tune them better and control more tightly the interaction between the bonus they provide.


### Perception range

A 120deg cone is projected in front of the person in the direction they are facing, as shown in this figure.

![field-of-view-image](https://github.com/arthur-hav/attf/blob/main/fov-diagram.PNG?raw=true)

Everything within it and within range is seen, unless the line center-to-center is obstructed by a wall. You may use a ruler for this check.

#### Backstabbing

Each character including players have their own separate perception range and can be backstabbed from any ennemy if:
- The backstabbing ennemy remains off vision of their target for their entire turn.
- They perform a ranged or melee attack (or martial art) on the target.

They take on this occasion a bonus to their base attack damage equal to their backstab bonus damage %. This can be further modified by martial art used.

Example: Player is playing a cloak and dagger assassin. They have 6 base damage and are granted 200% backstab bonus due to their class and equipment. 
They manage to sneak around an opponent and use "power attack", a martial art that takes 3 action point but deals 150% damage. Their target don't have armor.

Their target take 150% * 300% * 6 = 27 points of damage.

### Actions

#### Resolution of attacks and special abiliites

Attacks and special abilities unless otherwise specified take 2 actions points to complete.

They automatically succeed, however they can be less effective based on armor or endurance of the target. The formulae is: 

```Result offense = (10 * base offense score) / (10 + defense score)``` -> Rounded to nearest integer.

Stanidng in high ground or cover advantage over the attacker give +2 to armor and endurance.

The following table shows how much a given amount of defense reduce a given offense:

```
  Def: | 1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
--------------------------------------------------------------------
Off 1  | 1  1  1  1  1  1  1  1  1  0  0  0  0  0  0  0  0  0  0  0 
Off 2  | 2  2  2  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1 
Off 3  | 3  2  2  2  2  2  2  2  2  2  1  1  1  1  1  1  1  1  1  1 
Off 4  | 4  3  3  3  3  2  2  2  2  2  2  2  2  2  2  2  1  1  1  1 
Off 5  | 5  4  4  4  3  3  3  3  3  2  2  2  2  2  2  2  2  2  2  2 
Off 6  | 5  5  5  4  4  4  4  3  3  3  3  3  3  2  2  2  2  2  2  2 
Off 7  | 6  6  5  5  5  4  4  4  4  4  3  3  3  3  3  3  3  2  2  2 
Off 8  | 7  7  6  6  5  5  5  4  4  4  4  4  3  3  3  3  3  3  3  3 
Off 9  | 8  8  7  6  6  6  5  5  5  4  4  4  4  4  4  3  3  3  3  3 
Off 10 | 9  8  8  7  7  6  6  6  5  5  5  5  4  4  4  4  4  4  3  3 
Off 11 | 10 9  8  8  7  7  6  6  6  6  5  5  5  5  4  4  4  4  4  4 
Off 12 | 11 10 9  9  8  8  7  7  6  6  6  5  5  5  5  5  4  4  4  4 
Off 13 | 12 11 10 9  9  8  8  7  7  6  6  6  6  5  5  5  5  5  4  4 
Off 14 | 13 12 11 10 9  9  8  8  7  7  7  6  6  6  6  5  5  5  5  5 
Off 15 | 14 12 12 11 10 9  9  8  8  8  7  7  7  6  6  6  6  5  5  5 
Off 16 | 15 13 12 11 11 10 9  9  8  8  8  7  7  7  6  6  6  6  6  5 
Off 17 | 15 14 13 12 11 11 10 9  9  8  8  8  7  7  7  7  6  6  6  6 
Off 18 | 16 15 14 13 12 11 11 10 9  9  9  8  8  8  7  7  7  6  6  6 
Off 19 | 17 16 15 14 13 12 11 11 10 10 9  9  8  8  8  7  7  7  7  6 
Off 20 | 18 17 15 14 13 12 12 11 11 10 10 9  9  8  8  8  7  7  7  7 
```

Example: Player cast fire bolt which deals 150% special damage to a single target. They have 7 base special power.

This target is covered with 1 endurance point, so their total defense is 3. 7 offense vs 3 results in 5 offense => the player gets to deal 5 * 150% = 7 damage with their fire bolt.

#### Attacks and martial arts

A basic attack is an action done by the player that inflicts their base damage to an enemy in range

Martial arts is a kind of special special that rely on attack damage instead of special power, deal damage with the main weapon of the character so use armor as defense characteristics instead of endurance.

Disarming debuff prevents martial art but debuffs that affect specials ("silences") should not prevent martial art.

Like any special, martial art may include effects similar to specials.

#### Special and effects

Casting a spell usually should take 2 actions and expand energy in order to provide a better-than-basic-attack effect.

These effects can be any from the following list:

- Straight damage, reduced by the target endurance
- Straight heal, without any modifier to special power
- Buff or debuff to any of the characteristics (ex: movement point, action, perception, armor...)
- Creating or modifying terrain
- Giving temporary hit points (shield)
- Remove any temporary debuffs
- Remove an option for the target for spending their action: disabling movement, attacks, or specials.
- Grant immunity against a given class of effects
- Move and teleport self, allies or ennemies
- Place a ground marker that effects on stepping on it (trap), on starting turn on it (e.g. healing beacon) or in the beginning of the next turn (delayed meteor fall)
- Invoke a creature

Unlike other actions, specials and martial arts can include cooldown. Players should track the cooldown of their abilities so to not cast more than allowed their specials or martial art.

The game master is encouraged to create their own special corresponding to their universe although some specials will be provided as being taken as-is or, after 
experimentation, as an example balance of what is expected from specials.

Magic can be divided into major elements or schools in order to avoid characters to collect an overpowered collection of spells, and keep complementary balance between casters.

An example of such separation: the 4 major elements Fire / water / air / earth, with corresponding descriptions.

- Fire: Destructive fire magic, offensive combat buffs
- Water: Ice magic, combat debuffs and curses
- Air: Thunder magic, illusion magic, mobility magic
- Earth: Magic inflicting physical damage, terrain manipulation, defensive combat buffs