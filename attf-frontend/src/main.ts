import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
      {
        path: '/:gameid',
        name: 'home',
        component: App
      },
    ]
  })
const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
