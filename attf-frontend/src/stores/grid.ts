import { defineStore } from 'pinia'
import { reactive } from 'vue'
import action1 from '../assets/action1.png'
import action2 from '../assets/action2.png'

function real_dist(x1: number, y1: number, x2: number, y2: number) {
  return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
}

function dist(x: number, y: number, x2: number, y2: number) {
  if (y % 2 == 1) {
    x += 0.5
  }
  if (y2 % 2 == 1) {
    x2 += 0.5
  }
  return Math.max(Math.abs(y - y2), Math.floor(Math.abs(x - x2) + Math.abs(y - y2) / 2))
}

function _findVisible(creatures: { [key: number]: CreatureData }, t: TileData) {
  for (const [hashval, cr] of Object.entries(creatures)) {
    for (var tHash of cr.visibleTiles) {
      if (tHash == t.hash()) {
        return true
      }
    }
  }
  return false
}


function setGrid(store: any, gridKey: string | null, gridJson: any) {
  if (gridKey != null) {
    if (!store.grid[gridKey]) {
      store.grid[gridKey] = new TileData(gridJson.y, gridJson.x)
    }
    store.grid[gridKey].fromObject(gridJson)
  }
  else {
    store.grid = []
    for (const [hashval, tileObj] of Object.entries(gridJson)) {
      store.grid[hashval] = new TileData(tileObj.y, tileObj.x)
      store.grid[hashval].fromObject(tileObj)
    }
  }
  for (const [hashval, td] of Object.entries(store.grid)) {
    store.maxX = Math.max(store.maxX, td.x)
    store.maxY = Math.max(store.maxY, td.x)
  }
}


function setCreatures(store: any, creatureKey: string | null, creatureJson: any) {
  if (creatureKey != null) {
    store.creatures[creatureKey] = new CreatureData(creatureJson.avatar, creatureJson.id, creatureJson.tilehash)
    store.creatures[creatureKey].fromObject(creatureJson)
  }
  else {
    store.creatures = {}
    for (const [hashval, crObj] of Object.entries(creatureJson)) {
      store.creatures[hashval] = new CreatureData(crObj.avatar, crObj.id, crObj.tilehash)
      store.creatures[hashval].fromObject(crObj)
    }
  }
  for (const [hashval, cr] of Object.entries(store.creatures)) {
    store.creaturecnt = Math.max(store.maxX, cr.id)
  }
}


function onmessage(event: any) {
  //console.log(event.data)
  const messageObj = JSON.parse(event.data)
  const grid = useGridStore()
  for (var line of messageObj) {
    if (line[0] == 'set') {
      if (!line[1].length) {
        setCreatures(grid, null, line[2].creatures)
        setGrid(grid, null, line[2].grid)
        grid.onTrait = line[2].onTrait
      }
      else if (line[1][0] == 'creatures') {
        setCreatures(grid, line[1][1], line[2])
      }
      else if (line[1][0] == 'onTrait') {
        grid.onTrait = line[2]
      }
      else if (line[1][0] == 'grid') {
        setGrid(grid, line[1][1], line[2])
      }
    }
    if (line[0] == 'unset') {
      if (line[1][0] == 'creatures') {
        if (line[1][1]) {
          const hash = line[1][1]
          delete grid.creatures[hash]
        }
        else {
          grid.creatures = {}
        }
      }
    }
  }
};

export const useMouseInfoStore = defineStore('mouseinfo', {
  state() {
    var tileDragged: null | TileData = null
    var tileSelected: null | TileData = null
    var dragAction: null | ActionData = null
    var dragStatus: null | StatusData = null
    var dragSpawn: null | string = null
    var onHighlight = function () { };
    var mode: string = "normal"
    return { tileDragged, tileSelected, dragAction, dragSpawn, mode, onHighlight, dragStatus }
  },
  getters: {
    draggedCreature(state) {
      if (state.tileDragged === null) return null
      const gridStore = useGridStore()
      return gridStore.creatures[state.tileDragged.hash()]
    },
    clickSelectedCreature(state) {
      if (state.tileSelected === null) return null
      const gridStore = useGridStore()
      return gridStore.creatures[state.tileSelected.hash()]
    },
  },
  actions: {

  },
})



export const useGridStore = defineStore('grid', {
  state: () => {
    var grid: { [key: number]: TileData } = reactive({})
    var maxX: number = 0
    var maxY: number = 0
    var creaturecnt: number = 0
    var onTrait: number = 0
    var creatures: { [key: number]: CreatureData } = reactive({})

    function check_angle(x: number, y: number, x2: number, y2: number, direction: string) {
      if (x == x2 && y == y2) {
        return true
      }
      if (y % 2 == 1) {
        x += 0.5
      }
      if (y2 % 2 == 1) {
        x2 += 0.5
      }
      return check_angle_real(x, y, x2, y2, direction)
    }
    function check_angle_real(x: number, y: number, x2: number, y2: number, direction: string) {
      if (direction == 'right')
        return x - x2 >= 0 && Math.abs(y - y2) / 2 <= (x - x2)
      if (direction == 'left')
        return x - x2 <= 0 && Math.abs(y - y2) / 2 <= -(x - x2)
      if (direction == 'top-left')
        return y - y2 <= 0 && (y - y2) / 2 <= -(x - x2)
      if (direction == 'bot-left')
        return y - y2 >= 0 && (y - y2) / 2 >= (x - x2)
      if (direction == 'top-right')
        return y - y2 <= 0 && (y - y2) / 2 <= (x - x2)
      if (direction == 'bot-right')
        return y - y2 >= 0 && (y - y2) / 2 >= -(x - x2)
      return true
    }

    //var ws = new WebSocket('wss://' + location.host + "/api/" + location.pathname);

    var ws = new WebSocket('ws://localhost:8181' + location.pathname);
    ws.onmessage = onmessage
    return { grid, creatures, creaturecnt, onTrait, maxX, maxY, ws, check_angle, check_angle_real }
  },
  getters: {
    creaturesByInitiative(state) {
      const creatures = Object.values(state.creatures)
      creatures.sort((c1, c2) => 100 * c2.initiative + c2.maxHealth - 100 * c1.initiative - c1.maxHealth)
      return creatures
    },
    rows(state) {
      var rows: TileData[][] = []
      for (var i = 0; i < state.maxY; i++) {
        rows.push([])
        for (var j = 0; j < state.maxX; j++) {
          var tile = state.grid[1000000 * i + j]
          if (tile != null) {
            rows[i].push(tile)
          }
        }
      }
      return rows
    },
    /*  
      isWallRightVisible(state) {
        return (y: number, x: number) => {
          const foundOrigin = _findVisible(state.creatures, y, x)
          if (foundOrigin){
            return true
          }
          return _findVisible(state.creatures, y, x + 1)
        }  }, 
      isWallTopLeftVisible(state) 
      {
        return (y: number, x: number) => {
          const foundOrigin = _findVisible(state.creatures, y, x)
          if (foundOrigin){
            return true
          }
          return _findVisible(state.creatures, y-1, x - (y % 2 == 0 ? 1 : 0))
        }
      }, 
      isWallTopRightVisible(state) {
        return (y: number, x: number) => {
          const foundOrigin = _findVisible(state.creatures, y, x)
          if (foundOrigin){
            return true
          }
          return _findVisible(state.creatures, y-1, x + (y % 2 == 0 ? 0 : 1))
        }
      }
      */
  },
  actions: {
    setViewDirection(cr: CreatureData, lookAt: TileData) {
      var diff_y = lookAt.y - cr.tiledata().y
      var diff_x = lookAt.x - cr.tiledata().x
      if (cr.tiledata().y % 2 == 1) {
        diff_x -= 0.5
      }
      if (lookAt.y % 2 == 1) {
        diff_x += 0.5
      }
      if (diff_x > 0) {
        if (Math.abs(diff_y) < Math.abs(diff_x) / 1.5) {
          cr.direction = 'right'
        }
        else if (diff_y < 0) {
          cr.direction = 'top-right'
        }
        else {
          cr.direction = 'bot-right'
        }
      }
      else {
        if (Math.abs(diff_y) < Math.abs(diff_x) / 1.5) {
          cr.direction = 'left'
        }
        else if (diff_y < 0) {
          cr.direction = 'top-left'
        }
        else {
          cr.direction = 'bot-left'
        }
      }
    },
    updateView(creature: CreatureData) {
      creature.visibleTiles = []
      const x = creature.tiledata().x
      const y = creature.tiledata().y
      const x2 = x * 68 + (y % 2 == 0 ? 0 : 34)
      const y2 = y * 60
      const per = creature.perception
      var visibleWalls = []
      var visibleSeps = []
      for (const [hashval, tile] of Object.entries(this.grid)) {
        if (dist(tile.x, tile.y, x, y) <= per) {
          if (this.check_angle(tile.x, tile.y, x, y, creature.direction)) {
            creature.visibleTiles.push(tile.hash())
            if (tile.isWall) {
              visibleWalls.push(tile)
            }
            var cur_y = tile.y * 60
            var cur_x = tile.x * 68 + (tile.y % 2 == 0 ? 0 : 34)
            if (tile.wallRight && (this.check_angle_real(cur_x + 34, cur_y - 20, x2, y2, creature.direction) || this.check_angle_real(cur_x + 34, cur_y + 20, x2, y2, creature.direction))) {
              visibleSeps.push([cur_y - 20, cur_x + 34, cur_y + 20, cur_x + 34])
            }
            if (tile.wallTopLeft && (this.check_angle_real(cur_x - 34, cur_y - 20, x2, y2, creature.direction) || this.check_angle_real(cur_x, cur_y - 40, x2, y2, creature.direction))) {
              visibleSeps.push([cur_y - 20, cur_x - 34, cur_y - 40, cur_x])
            }
            if (tile.wallTopRight && (this.check_angle_real(cur_x, cur_y - 40, x2, y2, creature.direction) || this.check_angle_real(cur_x + 34, cur_y - 20, x2, y2, creature.direction))) {
              visibleSeps.push([cur_y - 40, cur_x, cur_y - 20, cur_x + 34])
            }
          }
        }
      }
      for (var t = 0; t < creature.visibleTiles.length; t++) {
        const vTileData = this.grid[creature.visibleTiles[t]]
        for (var s = 0; s < visibleSeps.length; s++) {
          const tile_x = vTileData.x * 68 + (vTileData.y % 2 == 0 ? 0 : 34)
          const tile_y = vTileData.y * 60
          const [sep_y1, sep_x1, sep_y2, sep_x2] = visibleSeps[s]

          if (real_dist(x2, y2, sep_x1, sep_y1) >= real_dist(x2, y2, tile_x, tile_y) &&
            real_dist(x2, y2, sep_x2, sep_y2) >= real_dist(x2, y2, tile_x, tile_y)
          ) {
            continue
          }
          const denom = (sep_x1 - sep_x2) * (tile_y - y2) - (sep_y1 - sep_y2) * (tile_x - x2)
          if (denom == 0) {
            continue
          }
          const inter_x = ((sep_x1 * sep_y2 - sep_y1 * sep_x2) * (tile_x - x2) - (sep_x1 - sep_x2) * (tile_x * y2 - tile_y * x2)) / denom
          const inter_y = ((sep_x1 * sep_y2 - sep_y1 * sep_x2) * (tile_y - y2) - (sep_y1 - sep_y2) * (tile_x * y2 - tile_y * x2)) / denom
          if (inter_x < Math.max(sep_x1, sep_x2) + 2 && inter_x > Math.min(sep_x1, sep_x2) - 2 && inter_y < Math.max(sep_y1, sep_y2) + 2 && inter_y > Math.min(sep_y1, sep_y2) - 2) {
            creature.visibleTiles.splice(t, 1)
            t -= 1
            break
          }
        }
      }
      var usedWalls = []
      visibleWalls.sort((t1, t2) => dist(x, y, t1.x, t1.y) - dist(x, y, t2.x, t2.y))
      for (var t = 0; t < creature.visibleTiles.length; t++) {
        const vTileData = this.grid[creature.visibleTiles[t]]
        for (var w = 0; w < visibleWalls.length; w++) {
          const tile_x = vTileData.x * 68 + (vTileData.y % 2 == 0 ? 0 : 34)
          const tile_y = vTileData.y * 60
          const wall_x = visibleWalls[w].x * 68 + (visibleWalls[w].y % 2 == 0 ? 0 : 34)
          const wall_y = visibleWalls[w].y * 60

          if (dist(x, y, visibleWalls[w].x, visibleWalls[w].y) >= dist(x, y, vTileData.x, vTileData.y)) {
            continue
          }

          const dist_point_to_line = Math.abs(
            (tile_x - x2) * (y2 - wall_y)
            - (x2 - wall_x) * (tile_y - y2))
            / Math.sqrt((tile_x - x2) * (tile_x - x2) +
              (tile_y - y2) * (tile_y - y2))
          if (dist_point_to_line <= 38) {
            creature.visibleTiles.splice(t, 1)
            usedWalls.push(w)

            t -= 1
            break
          }
        }
      }
      for (var w = 0; w < usedWalls.length; w++) {
        for (var tHash of creature.visibleTiles) {
          if (this.grid[tHash] == visibleWalls[usedWalls[w]]) {
            break
          }
        }
        creature.visibleTiles.push(visibleWalls[usedWalls[w]].hash())
      }
      this.ws.send(JSON.stringify([['set', ['creatures', creature.tilehash], creature]]))
    },
    deleteCreature(draggedHash: number) {
      for (const [hashval, cr] of Object.entries(this.creatures)) {
        if (parseInt(hashval) == draggedHash) {
          this.ws.send(JSON.stringify([['unset', ['creatures', hashval]]]))
          return
        }
      }
    },
    addCreatureAt(avatar: string, tiledata: TileData) {
      this.creaturecnt += 1
      const cr = new CreatureData(avatar, this.creaturecnt, tiledata.hash())
      this.updateView(cr)
      this.ws.send(JSON.stringify([['set', ['creatures', cr.tilehash], cr]]))
    },
    moveCreature(cr: CreatureData, newTile: TileData) {
      const swp = this.creatures[newTile.hash()]
      const old_data = cr.tilehash
      if (swp) {
        cr.tilehash = swp.tilehash
        swp.tilehash = old_data
        this.creatures[cr.tilehash] = swp
        this.updateView(swp)
        this.updateView(cr)
      }
      else {
        cr.tilehash = newTile.hash()
        this.updateView(cr)
        this.ws.send(JSON.stringify([['unset', ['creatures', old_data]]]))
      }
    },
    pushAction(cr: CreatureData, imgUrl: string, actionText: string) {
      cr.actionData.push(new ActionData(imgUrl, actionText))
      this.ws.send(JSON.stringify([['set', ['creatures', cr.tilehash], cr]]))
    },
    pushStatus(cr: CreatureData, imgUrl: string, actionText: string, duration: number) {
      cr.statusData.push(new StatusData(imgUrl, actionText, duration))
      this.ws.send(JSON.stringify([['set', ['creatures', cr.tilehash], cr]]))
    }
  }
})


export class TileData {
  x: number
  y: number
  isWall: boolean
  wallTopLeft: boolean
  wallTopRight: boolean
  wallRight: boolean
  constructor(y: number, x: number) {
    this.x = x
    this.y = y
    this.isWall = false
    this.wallTopLeft = false
    this.wallTopRight = false
    this.wallRight = false
  }
  hash() {
    return this.x + 1000000 * this.y
  }
  getCreature() {
    const store = useGridStore()
    return store.creatures[this.hash()]
  }
  isVisible() {
    const grid = useGridStore()
    for (const [hashval, cr] of Object.entries(grid.creatures)) {
      if (dist(cr.tiledata().x, cr.tiledata().y, this.x, this.y) > cr.perception) {
        continue
      }
      for (var visibleHash of cr.visibleTiles) {
        if (this.hash() == visibleHash) {
          return true
        }
      }
    }
    return false
  }
  fromObject(obj: Object) {
    this.x = obj.x
    this.y = obj.y
    this.isWall = obj.flags.indexOf('is-wall') != -1
    this.wallTopLeft = obj.flags.indexOf('wall-top-left') != -1
    this.wallTopRight = obj.flags.indexOf('wall-top-right') != -1
    this.wallRight = obj.flags.indexOf('wall-right') != -1
  }
  toNetwork() {
    var obj: Object = { x: this.x, y: this.y, flags: [] }
    if (this.isWall) obj.flags.push('is-wall')
    if (this.wallTopLeft) obj.flags.push('wall-top-left')
    if (this.wallTopRight) obj.flags.push('wall-top-right')
    if (this.wallRight) obj.flags.push('wall-right')
    return obj
  }
}

export class CreatureData {
  direction: string
  health: number
  maxHealth: number
  energy: number
  maxEnergy: number
  energyRegen: number
  avatar: string
  isPc: boolean
  perception: number
  initiative: number
  tilehash: number
  visibleTiles: number[]
  id: number
  actionData: ActionData[]
  statusData: StatusData[]
  constructor(avatar: string, id: number, tilehash: number) {
    this.id = id
    this.tilehash = tilehash
    this.avatar = avatar
    this.health = 30
    this.energy = 25
    this.energyRegen = 2
    this.maxEnergy = 25
    this.maxHealth = 30
    this.direction = 'right'
    this.isPc = true
    this.perception = 7
    this.initiative = 5
    this.visibleTiles = []
    this.actionData = [new ActionData(action2, 'Move'), new ActionData(action1, 'Attack')]
    this.statusData = []
  }
  tiledata() {
    var grid = useGridStore()
    return grid.grid[this.tilehash]
  }
  fromObject(obj: any) {
    this.actionData = []
    for (var aData of obj.actionData) {
      this.actionData.push(new ActionData(aData.imgUrl, aData.tooltipText))
    }
    for (var sData of obj.statusData) {
      this.statusData.push(new StatusData(sData.imgUrl, sData.tooltipText, sData.duration))
    }
    this.health = obj.health
    this.energy = obj.energy
    this.maxEnergy = obj.maxEnergy
    this.maxHealth = obj.maxHealth
    this.direction = obj.direction
    this.energyRegen = obj.energyRegen
    this.perception = obj.perception
    this.isPc = obj.isPc
    this.initiative = obj.initiative
    this.visibleTiles = obj.visibleTiles
  }
}

export class ActionData {
  imgUrl: string
  tooltipText: string
  constructor(imgUrl: string, tooltipText: string) {
    this.imgUrl = imgUrl
    this.tooltipText = tooltipText
  }
}

export class StatusData {
  imgUrl: string
  tooltipText: string
  duration: number
  constructor(imgUrl: string, tooltipText: string, duration: number) {
    this.imgUrl = imgUrl
    this.tooltipText = tooltipText
    this.duration = duration
  }
}